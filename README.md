### 郑重声明  

近期发现市场上有人冒充启山智软工作人员通过售卖盗版启山智软社区团购物流配送系统源码非法获益，在此我们郑重声明：非经过本公司官方渠道购买的启山智软社区团购物流配送系统源码出现任何问题我们概不负责。

使用违法获取的盗版系统，存在很多的数据安全隐患，还会带来法律风险，请大家共同监督，发现人员出售盗版系统，及时与我们联系，我们将依法追究其法律责任 :exclamation:  :exclamation:  :exclamation: 


|交流群   |技术微信   |技术微信   |
|---|---|---|
|![输入图片说明](images/%E7%A4%BE%E5%8C%BA.png)   |![输入图片说明](images/%E6%9C%AA%E6%A0%87%E9%A2%98-1.png)   |![输入图片说明](images/%E6%9D%A8%E5%9D%A4-.png)|
|QQ：556731103   |VX:18967889883   |VX:18067433978   |
|   |   |   |


### [前言](http://www.bgniao.cn)
启山智软社区团购系统是一款系统稳定且经过线上反复论证并拥有大量真实用户使用的Java社区团购配送系统。

基于市场的反馈和变化，我们在不断开发完善社区团购的基础上，增加属于我们自己的物流配送模块，来帮助线下门店针商品系统下单，批量出单，合理分配，精准配送，在投放各大门店使用后，针对实际情况中出现的各种问题，我们不断的改进，收获了大家的好评和喜欢。

我们由衷希望启山智软社区团购系统可以通过gitee平台让更多的人了解到好的产品。同时欢迎大家积极交流沟通，如有不足之处多给我们的项目提意见或建议，实现共同进步。
![输入图片说明](images/6.jpg)

### 相关链接

物流配送	: https://www.bgniao.cn

公司官网	: https://www.bgniao.cn

更新文档: https://www.bgniao.cn/notice

商家后台: https://group.bgniao.cn/copartner/1.0.1/sign  账号：13157479071 密码：admin123

区域团长后台: https://group.bgniao.cn/areaColonel/overview

商务对接wx/电话:18067433978-18967889883   


### 项目介绍

启山智软社区团购是基于Spring Cloud 和 Vue.js的JAVA系统。包含总控制后台 、城市合伙人(商家pc端)、团长/区域团长/提货点后台 、用户端小程序 、手机H5等多个操作模块。为响应用户需求我们新增了后台DIY装修拖拽式组件，淘宝商品CSV一键导入，还有与众不同的管理台侧边栏设计。可支持二开，私有化部署，需求功能定制。

### 必读

如需深入了解启山智软社区团购物流配送系统功能信息，可添加商务负责人 **微信：18067433978-18967889883** 获取测试账号进行体验。

开发者需知: 启山智软社区团购物流配送系统我们只开放相关文档及项目介绍。如需咨询源码相关情况请添加负责人 **微信：18067433978-18967889883** ，本公司技术人员可提供相关操作使用教程相应资料。

如有公益项目需使用启山智软社区团购物流配送系统，我司愿提供免费商家入驻及一切售后服务来奉献力所能及的爱心，公益项目需提供相关证明,我司将拥有针对该项的最终解释权。  

### 项目演示

| 小程序演示 | 操作流程 |
|-------|------|
|![](images/dfa38c178d478e0e970fe2e3530255d.jpg)|![](https://medusa-small-file-1253272780.cos.ap-shanghai.myqcloud.com/gitee/5051.gif "启山智软社区团购操作流程")      |


### 荣誉资质
|![输入图片说明](https://images.gitee.com/uploads/images/2021/0908/150857_e5b71af2_8533008.png "微信图片_社区团购资质证书1.png")|![输入图片说明](https://images.gitee.com/uploads/images/2021/0908/151131_413737cf_8533008.png "微信图片_布谷鸟资质证书.png")|![输入图片说明](https://images.gitee.com/uploads/images/2021/0814/104233_f71a9b70_8533008.png "外观专利.png")|
|---|---|---|


### 技术选型

| 技术                   | 说明                 | 官网                                                 |
| ---------------------- | -------------------- | ---------------------------------------------------- |
| Spring Cloud           | 微服务框架           | https://spring.io/projects/spring-cloud              |
| Spring Cloud Alibaba   | 微服务框架           | https://github.com/alibaba/spring-cloud-alibaba      |
| Spring Boot            | 容器+MVC框架         | https://spring.io/projects/spring-boot               |
| MyBatis-Plus           | 数据层代码生成       | http://www.mybatis.org/generator/index.html          |
| Swagger                | 文档生成工具         | https://swagger.io/     
|                                                                                                     |
| RabbitMq               | 消息队列             | https://www.rabbitmq.com/                            |
| Redis                  | 分布式缓存           | https://redis.io/                                    |
| Druid                  | 数据库连接池         | https://github.com/alibaba/druid                     |
| OSS                    | 对象存储             | https://github.com/aliyun/aliyun-oss-java-sdk        |
| JWT                    | JWT登录支持          | https://github.com/jwtk/jjwt                         |
| XXL-JOB                | 分布式任务调度平台   |https://www.xuxueli.com/xxl-job/                       |
|                                                                                                     |
| Lombok                 | 简化对象封装工具     | https://github.com/rzwitserloot/lombok               |
| Docker                 | 应用容器引擎         | https://www.docker.com/                              |          
|Sonarqube				 | 代码质量控制	        |https://www.sonarqube.org/
|                                                                                                     |
| element                | 组件库         | https://element.eleme.cn/#/zh-CN                           |
| Vue.js                 | 渐进式JavaScript 框架       | https://cn.vuejs.org/                         |
| Vue-router 			 | 前端路由 		       | https://router.vuejs.org/zh/ 	                            |
| vuex 					 | 状态管理            | https://vuex.vuejs.org/zh/ 								|
| modeuse-core 			 | 自主开发UI组件       | -- 													|
| TypeScript             | JavaScript超集       | https://www.tslang.cn/                    
|                        |
| eslint             	 | 代码质量控制         | https://eslint.org/                                   |                 
| hook	             	 | 代码质量控制         |                                                       |
                 

### 系统架构图
![](https://bgniao-small-file-1253272780.cos.ap-chengdu.myqcloud.com/group_purchase_open/sasasa1.png "布谷鸟社区团购架构图")

### 功能模块
基础服务：

- oss对象存储
- sms短信服务
- 支付服务
- 总台服务

功能服务：

- 商品服务	
- 订单服务	 
- 直播服务
- DIY装修
- 数据服务
- 配送服务
- 实时概况
- 营销应用
- 财务管理
- 会员管理




## [更新详细说明](https://www.bgniao.cn/notice) https://www.bgniao.cn/

### B/S 社区团购商家端功能清单

- 经营概况：实时概况、交易概况、排行榜等；
		
- 商品管理：自定义商品专区、产品列表 、csv素材导入等；
				
- 订单管理：快递订单、社区订单、订单评价管理等；

- 社群拼团：拼团活动、团长管理、区域团长管理等；

- 营销应用：优惠券、满减、积分商城、直播、社群接龙等；

- 财务管理：对账单、提现工单等；

- 客户管理：客户列表、会员管理、黑名单等；

- 配送方式：快递配送、社区配送等；
	  
- 商城设置：交易设置、支付设置、通用设置等；



 **允许** 

✅ 个人学习；

✅ 公益项目（请注明来源）。


🚫 **禁止** 
公有云厂商将其商业化。



## 角色说明
![]( https://bgniao-small-file-1253272780.cos.ap-chengdu.myqcloud.com/group_purchase_open/platform.png "角色说明")


## 用户端小程序页面展示
![](https://images.gitee.com/uploads/images/2021/1123/110110_34c159a5_8533008.jpeg "未标题.jpg")
## 商家端页面展示
![输入图片说明](images/gitee.png)

####  中石化
> 基于疫情影响和油费的上涨，再加上电动汽车市场的抨击，中国石化发展了自己的第二事业，想要发动所有店面员工售卖日需品，以店员为团长发动本小区和加油的客户注册会员并团购产品享有超值优惠价，不间断的小程序直播推广，一上线便覆盖两千多名团长，十几万用户量。

| ![输入图片说明](images/123.jpg)  |![输入图片说明](images/456.jpg)   |  ![输入图片说明](images/789.jpg) | ![输入图片说明](images/101112.jpg)  |
|---|---|---|---|

####  良田拾度
> 公司业务线覆盖整个餐饮行业包含前期的原材料种植到菜品生成，基于公司现有业务的发展和新形态的需求，该公司技术员联系到我们需要一套社区团购产品，以线上社区团购模式进行售卖，现以售卖胚芽米为主打产品，结合自有业务需求在短短一个月自行开发了功能并发布上线，上线后成功招募上千名分销员并推广开来，在本地运营的小有名气。

|  ![输入图片说明](images/111.jpg) |![输入图片说明](images/222.jpg)   | ![输入图片说明](images/333.jpg)  |![输入图片说明](images/444.jpg)   |
|---|---|---|---|

